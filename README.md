# Web exercises
The web exercise is divided into 4 parts differentiated by difficulty (from lower to higher), being the first of these explained during the practical session in the classroom by the teacher, the rest must be solved by the students through research and testing on the platform.
This practice simulates a web project that contains several vulnerabilities, each part simulates the review of the project where the creators of the service have solved the most obvious vulnerability, which has been reported, reaching the point where only a single vulnerability remains to be exploited.

This practice requires having the Docker package installed, and that the user to be used for its execution has permissions to create, delete and run containers (belonging to the "Docker" user group).

Each practice contains a "start.sh" file that will stop any Docker from the web practices, generate the new Docker and run it, exposing port 80 of the container on port 800 of the host.

## Web1 - Guided
TechIT developers have asked us to analyze their website for vulnerabilities to improve the development before going into production, could you analyze if the platform has significant vulnerabilities?


## Web2 - Basic
The developers of the web project we audited have just sent us a version where they have supposedly solved the problem whereby any file could be uploaded and allowed us to execute code remotely on the system, could you confirm that the solution provided is robust enough not to be exploitable?


## Web3 - Medium
After demonstrating to the developers that their development is still insecure, they have just applied some improvement and have provided us with a new version, this time they assure us that it is not possible to interpret the files that have been uploaded to the platform, can you confirm that indeed the development is now secure?


## Web4 - Hard
After notifying the development team, they have chosen to remove the file upload functionality as they did not know how to fix the problem, could you check if there is any other critical vulnerability on the web service they are developing?
