<?php
session_start();
?>
<html lang="en">
    <head>
    	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<title>TechIT Solutions</title>
	<link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
    	<header>
		<nav>
		    <div class="logo">
		        <img src="logo.png" alt="TechIT Solutions Logo">
		    </div>
		    <ul>
		        <li><a href="/index.php#home">Home</a></li>
		        <li><a href="/index.php#services">Services</a></li>
		        <li><a href="/index.php#about">About Us</a></li>
		        <li><a href="/index.php?view=contact.php">Contact</a></li>
		    </ul>
		</nav>
    	</header>
	<?php 

		if (!isset($_GET["view"]) || $_GET["view"] == "index.php") { 
			include('contents/home.html');
			include('contents/services.html');
			include('contents/about.html'); 
		} 
		else { 
			$file = "/var/www/html/" . $_GET["view"];
			if (file_exists($file)) {
				include $file;
			} 
			else{
				include('contents/home.html');
				include('contents/services.html');
				include('contents/about.html'); 
			}
		}  
	?>
	<footer>
        	<p>&copy; 2023 TechIT Solutions</p>
    	</footer>
	</body>
</html>
