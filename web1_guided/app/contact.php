<?php
	$status = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$status = "<p>Thanks for contacting us, we will read your mail soon.</p>";
	}
?>
	
	<section id="contact">
		<h2>Contact Us</h2>
		<?php
			if(empty($status)){
			echo <<<'CFORM'
				<p>We're here to assist you! Contact us for more information about our services.</p>
				<form method="post" action="/index.php?view=contact.php">
					<label for="name">Name:</label>
					<input type="text" id="name" name="name" required>

					<label for="email">Email:</label>
					<input type="email" id="email" name="email" required>
					
					<br><br>
					
					<label for="message">Message:</label>
					<textarea id="message" name="message" rows="8" cols="80" required></textarea>

					<input type="submit" value="Send">
				</form>
			CFORM;
			}
			else {
				echo $status;
			}
		?>
	</section>
