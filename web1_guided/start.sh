#!/bin/bash
app="docker.apache"
docker container stop web1 >> /dev/null && docker container rm web1 >> /dev/null
docker container stop web2 >> /dev/null && docker container rm web2 >> /dev/null
docker container stop web3 >> /dev/null && docker container rm web3 >> /dev/null
docker container stop web4 >> /dev/null && docker container rm web4 >> /dev/null
docker build -t ${app} .
docker run -d -p 800:80 \
  --name web1 \
   ${app}
